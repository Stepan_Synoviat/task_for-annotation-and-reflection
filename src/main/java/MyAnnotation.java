
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
int objectNamber();
String adress();
float area();
String ownerName() default "empty";
}
