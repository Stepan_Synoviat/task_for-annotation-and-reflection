import javax.xml.bind.SchemaOutputResolver;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.annotation.Annotation;

@MyAnnotation(objectNamber = 1, area = 250, adress = "Lviv, Luhanska 18 St.")
public class MyClass {
	@MyAnnotation(objectNamber = 1, area = 251, adress = "Lviv, Luhanska 18 St.")
	private int objectId;

	@MyAnnotation(objectNamber = 1, area = 252, adress = "Lviv, Luhanska 18 St.")
	private String name;

	public void printSomeText(String text){
		System.out.println("I print some text: "+ text);
	}

	public int getObjectId() {
		return objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@SecondAnnotation
	public void method1() {
		System.out.println("method№1");
	}
	@SecondAnnotation
	public int method2() {
		System.out.println("method№2");
		return 0;
	}
	@SecondAnnotation
	public String method3() {
		System.out.println("method№3");
		return "method3";
	}

	public static void invokeMethodsAbove() {
		int number = 1;
		MyClass my = new MyClass();
		Method [] methods = my.getClass().getDeclaredMethods();
		System.out.println(methods.length);
		for (Method m : methods
		     ) {
			SecondAnnotation secondAnnotation = m.getAnnotation(SecondAnnotation.class);
			if(secondAnnotation != null){
				try {
					m.invoke(my);
					System.out.println(number);
					number++;
				}
				catch (Exception e){
					e.printStackTrace();
				}
			}
		}

	}

	//print all declared fields from class
	@MyAnnotation(objectNamber = 1, area = 255, adress = "Lviv, Luhanska 18 St.")
	public static void doSomething() {
		MyClass myClass = new MyClass();
		Class cls = myClass.getClass();
		Field[] f = myClass.getClass().getDeclaredFields();
		for (Field field : f
		) {
			System.out.println(field);
		}
	}
	// change value of field
	public static void dealWitFields(){
		MyClass myClass = new MyClass();
		Class cls = myClass.getClass();
		int objectId = myClass.getObjectId();
		String name = myClass.getName();
		System.out.println(objectId+" "+name);//output 0null
		try{
			Field field = myClass.getClass().getDeclaredField("name");
			field.setAccessible(true);
			field.set(myClass, (String) "new value");
			name = (String) field.get(myClass);
		} catch(NoSuchFieldException | IllegalAccessException e){
			e.printStackTrace();
		}
		System.out.println(name);
	}

	//Print annotation value into console, show all classes and methods annotations
	@MyAnnotation(objectNamber = 1, area = 253, adress = "111")
	public static void myMeth() {
		MyClass ob = new MyClass();

		try {
			Annotation annos[] = ob.getClass().getAnnotations();

			System.out.println("All annotations for MyClass:");
			for (Annotation a : annos)
				System.out.println(a);

			Method m = ob.getClass().getMethod("myMeth");
			annos = m.getAnnotations();

			System.out.println("All annotations for myMeth:");
			for (Annotation a : annos)
				System.out.println(a);


		} catch (NoSuchMethodException exc) {
			System.out.println("Method Not Found.");
		}
	}
	// invoke method with paramater
		public static void invokeMethod(){
			MyClass myClass = new MyClass();
			Method[] methods = myClass.getClass().getDeclaredMethods();
			for (Method meth : methods
			) {
				System.out.println(meth.getName());
			}
			try {
				Method method = myClass.getClass().getDeclaredMethod("printSomeText", String.class);
				method.setAccessible(true);
				method.invoke(myClass, "some text");
			} catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}


	public static void main(String[] args) {
	//	MyClass.myMeth();
	//	MyClass.doSomething();
		MyClass.invokeMethod();
	//	MyClass.dealWitFields();

	}
}
